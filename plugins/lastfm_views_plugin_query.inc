<?php
// $Id;

/**
 * @file
 *   Views query plugin for the last.fm API.
 */

/**
 * Views query plugin for the last.fm API.
 *
 * This is where the magic happens. I have no idea if this is the best way to
 * go about things, it is just what made sense to me as I was putting this all
 * together. Comments and suggestions welcome.
 */
class lastfm_views_plugin_query extends views_plugin_query {
 
  // Our argument array. Anything that needs to be passed to the API gets
  // added to this array through the argumet and filter handlers. This array
  // then gets passed to url() along with $api_url and becomes the query 
  // string. 
  var $args;
  
  /**
   * Constructor; Create the basic query object and fill with default values.
   */
  function init($base_table, $base_field) { }

  function use_pager() {
    return FALSE;
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($view, $get_count = FALSE) {  }

  /**
   * Executes the query and fills the associated view object with according
   * values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   */
  function execute(&$view) {
    $start = microtime();
    
    // If we don't have a method, then just bail.
    if (empty($this->args['method'])) {
      return;
    }
    
    module_load_include('inc', 'lastfm', 'lastfm.api');
    $this->request = lastFmApiRequest::factory($this->args['method']);
    unset($this->args['method']);
    foreach ($this->args as $k => $v) {
      $this->request->setFilter($k, $v);
    }

    $this->request->execute();
    $data = $this->request->response->events;

    // When the last.fm api returns one event, it returns $events->event and that
    // object has the event data. When it returns more than one event, $events->event
    // is a keyed array of event data. Because of this, we need to make sure we iterate
    // from the right place or problems occur.
    if (!empty($data->event->id)) {
      // This is a single event
      $events->event = array($events->event);
    }

    // Iterate through the events and set result data as appropriate.
    // Unfortunately a lot of the returned data has to be munged in one
    // form or another.
    foreach ($data->event as $key => $event) {
      // Venue information is embedded in more arrays and objects, so we need to
      // flatten it out for our fields to be able to find it.
      $event->venue_latitude = $event->venue->location->{'geo:point'}->{'geo:lat'};
      $event->venue_longitude = $event->venue->location->{'geo:point'}->{'geo:long'};
      unset($event->venue->location->{'geo:point'});

      // Venue location information
      foreach ($event->venue->location as $venue_location_key => $value) {
        $event->{'venue_' . $venue_location_key} = $value;
      }
      unset($event->venue->location);

      // Venue images
      foreach ($event->venue->image as $value) {
        $event->{'venue_image_' . $value->size} = $value->{'#text'};
      }
      unset($event->venue->image);

      // Generic venue information
      foreach ($event->venue as $venue_key => $value) {
        $event->{'venue_' . $venue_key} = $value;
      }
      unset($event->venue);

      // Event images
      foreach ($event->image as $value) {
        $event->{'event_image_' . $value->size} = $value->{'#text'};
      }
      unset($event->image);
      
      // Convert startDate field to a Unix timestamp
      $event->startDate = strtotime($event->startDate);

      // Save ticketing information. When there is none, this comes through
      // as a string that can contain just blank spaces. When there is ticketing
      // information, it is an array with the ticket provider and a url. Can
      // this be multiple providers? It seems so but I've never seen it.
      if (is_object($event->tickets)) {
        $event->ticket_supplier = $event->tickets->ticket->supplier;
        $event->ticket_url = $event->tickets->ticket->{'#text'};        
      }
      unset($event->tickets);

      // Save our flattened array into the appropriate result row.
      $view->result[$key] = $event;
      
      // handle multi-instance fields
    }

    // Set row counter and execute time
    $view->total_rows = count($data->{'@attr'}->total);
    $view->execute_time = microtime() - $start;

    // Someday we may have a pager but not today
    $view->pager['current_page'] = 0;
    
  //  kpr($view);
  }
}
