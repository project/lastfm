<?php

/**
 * LastFM Request class.
 *
 * Use lastFmApiRequest::factory(my.methodname) to get a request object. All
 * public methods return $this and can be chained together.
 */
class lastFmApiRequest {

  // API response, populated by lastFmApiRequest->execute()
  public $response;
  
  // Flag to designate if the $response is from a cache.
  public $cache;

  // Default API url
  protected $url;

  // API parameter, use lastFmApiRequst->setFilter() to set.
  protected $params = array();

  // The cache table to use.
  protected $cache_table = 'cache'; 

  /**
   * Factory method!
   *
   * @param $method
   *   The Last.fm API method to build a request for.
   *
   * @return a new lastFmApiRequest object
   */
  static function factory($method) {
    return new lastFmApiRequest($method); 
  }

  /**
   * Constructor, please use the factory. k,thx
   */
  function __construct($method) {
    $this->url = variable_get('lastfm_api_url', LASTFM_DEFAULT_API_URL);
    $this->params['method'] = $method;
    $this->params['api_key'] = variable_get('lastfm_api_key', '');
  }

  /**
   * Add a filter to the request. Classes that extend this one should declare
   * what $keys are accepted.
   *
   * @param $key
   *   The filter to set
   * @param $value
   *   The value to set for the $key.
   *
   * @return the lastFmApiRequest object
   */
  public function setFilter($key, $value) {
    $this->params[$key] = $value;
    return $this;
  }

  /**
   * Sign the API request as a particular user.
   *
   * Note: the actually signature isn't generated until
   * lastFmApiRequest::execute() is called. Here we simply add the user's
   * session key to the request.
   * 
   * @param $account
   *   The user to sign the request as.
   *
   * @return the lastFmApiRequest object
   */
  public function sign($account = null) {
    if (!$account) {
      global $user;
      $account = $user;
    }

    if (module_exists('lastfm_user') && isset($user->lastfm_user)) {
      $this->params['sk'] = $user->lastfm_user['key'];
    }
    return $this;
  }

  /**
   * Make the request! The results of the call will be set on the `response`
   * attribute.
   *
   * @return the lastFmApiRequest object
   */
  public function execute() {
    // If this is an authenticated API call add the signature. This isn't done
    // in lasFmApiRequest::sign() so that it doens't need to freeze the params.
    if (isset($this->params['sk'])) {
      lastfm_user_sign($this->params);
    }

    // We cannot include the format in the signature, so we add it last.
    // @see http://www.last.fm/group/Last.fm+Web+Services/forum/21604/_/500629/1#f8513949
    $this->params['format'] = 'json';

    // Put the API call URL together.
    // TODO determine if we really should use url() here...
    $request_url = url($this->url, array('query' => $this->params, 'absolute' => TRUE));

    // Check if we have a cache hit or not.
    if ($result = $this->cache_get($request_url)) {
      $this->response = $result->data;
      $this->cache = TRUE;
    }
    else {
      $this->response = $this->request($request_url);
      $this->cache_set($request_url, $this->response);
      $this->cache = FALSE;
    }
    return $this;
  }

  /**
   * Make the actual HTTP request and parse output
   */
  protected function request($request_url, $l = 1) {
    // Limit to ten levels of recursion.
    if ($l > 10) {
     watchdog('warning', "Recusion limit reached.");
     return array();
    }
    $l++;

    $response = drupal_http_request($request_url);
    if ($response->code == '200') {
      $data = json_decode($response->data);
      if (is_object($data)) {
        if (isset($data->error)) {
          watchdog('error', "LastFM error !code recieved: %message", array('!code' => $data->error, '%mesage' => $data->message));
        }
        else {
          return $data;
          //
          // TODO determine how to handle the pager here. It seems like every
          // request returns a stdClass with a single attribute. This attribute
          // doesn't have a standard name, but varies by method. This single
          // attribute is also a stdClass, it has a @attr attribute that contains
          // the paging info.
          //

          /*
          list($head, $values) = $data;
          // A full reponse may be spread over several pages, so if there are
          // additional pages we retrieve them.
          if (($head->page < $head->pages) && ($head->per_page < $head->total)) {
            $this->params['page'] = $head->page + 1;
            $url = $this->url .'?'. $this->query_string();
            if ($recurse = $this->_request($url, $l)) {
              $values = array_merge($values, $recurse);
            }
          }
          return $values;
          */
        }
      }
      else {
        watchdog('error', "Didn't receive valid API response (invalid JSON).");
      }
    }
    else {
      watchdog('error', 'HTTP error !code received', array('!code' => $response->code));
    }
    return FALSE;
  }

  /**
   * Populate the cache. Wrapper around Drupal's cache_get()
   *
   * TODO determine the correct lifetime / management strategy for the response cache.
   * 
   * The caching model implemented here is somewhat different than drupal's
   * normal model because the data in this cache cannot be regenerated locally.
   *
   * Additionally we wait to avoid making repeated failed requests to the API in
   * the case where it's either down, or a invalid query has been fomulated.
   *
   * @param $url
   *   The API url that would be used.
   * @param $reset
   *   Set to TRUE to force a retrieval from the database.
   */
  protected function cache_get($request_url, $reset = FALSE) {
    static $items = array();

    $cid = $this->cache_id($request_url);
    if (!$items[$cid] || $reset) {
      $items[$cid] = cache_get($cid, $this->cache_table);
      // Don't return temporary items more that 5 minutes old.
      if ($items[$cid]->expire === CACHE_TEMPORARY && $items[$cid]->created > (time() + 300)) {
        return FALSE;
      }
    }
    return $items[$cid];
  }

  /**
   * Retrieve the cache. Wrapper around Drupal's cache_set()
   */
  protected function cache_set($url, $data) {
    if ($data === FALSE) {
      // If we don't get a response we set a temporary cache to prevent hitting
      // the API frequently for no reason.
      cache_set($this->cache_id($url), FALSE, $this->cache_table, CACHE_TEMPORARY);
    }
    else {
      $ttl = (int)variable_get('lastfm_cache_time', LASTFM_DEFAULT_CACHE_TIME);
      $expire = time() + ($ttl * 60);
      cache_set($this->cache_id($url), $data, $this->cache_table, $expire);
    }
  }

  /**
   * Helper function to generate a cache id based on the class name and
   * hash of the url
   */
  protected function cache_id($request_url) {
    return get_class($this) .':'. md5($request_url);
  }

}
